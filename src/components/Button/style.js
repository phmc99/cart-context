import styled from "styled-components";

export const Btn = styled.button`
  border: 0;
  border-radius: 5px;
  padding: 5px;
  font-size: 14px;
  background-color: #fcafca;
  cursor: pointer;
  width: 120px;

  &:hover {
    filter: brightness(0.85);
  }
`;
