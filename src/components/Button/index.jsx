import { useContext } from "react";
import { Btn } from "./style";
import { CartContext } from "../providers/cart";
import { CatalogueContext } from "../providers/catalougue";

const Button = ({ type, item }) => {
  const { addToCart, removeFromCart } = useContext(CartContext);
  const { addToCatalogue, removeFromCatalogue } = useContext(CatalogueContext);

  const text = type === "catalogue" ? "Add" : "Remove";

  const handleClick = () => {
    if (type === "catalogue") {
      removeFromCatalogue(item);
      addToCart(item);
    } else {
      removeFromCart(item);
      addToCatalogue(item);
    }
  };

  return <Btn onClick={handleClick}>{text}</Btn>;
};

export default Button;
