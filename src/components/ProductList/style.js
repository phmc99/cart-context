import styled from "styled-components";

export const List = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: column;
  gap: 10px;
  margin-top: 20px;

  li {
    background-color: #f3f3f3;
    display: flex;
    justify-content: space-between;
    padding: 10px;
    border-radius: 5px;
  }
`;
