import Button from "../Button";

import { useContext } from "react";

import { CatalogueContext } from "../providers/catalougue";
import { CartContext } from "../providers/cart";
import { List } from "./style";

const ProductList = ({ type }) => {
  const { catalogue } = useContext(CatalogueContext);
  const { cart } = useContext(CartContext);

  return (
    <List>
      {type === "catalogue" &&
        catalogue.map((item, index) => (
          <li key={index}>
            {item.name} <Button type={type} item={item} />
          </li>
        ))}
      {type === "cart" &&
        cart.map((item, index) => (
          <li key={index}>
            {item.name} <Button type={type} item={item} />
          </li>
        ))}
    </List>
  );
};

export default ProductList;
