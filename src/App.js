import ProductList from "./components/ProductList";
import { CatalogueProvider } from "./components/providers/catalougue";
import { CartProvider } from "./components/providers/cart";
import "./style.css";

const App = () => {
  return (
    <div className="App">
      <CatalogueProvider>
        <CartProvider>
          <h1>Products</h1>
          <ProductList type="catalogue" />
          <h1>Cart</h1>
          <ProductList type="cart" />
        </CartProvider>
      </CatalogueProvider>
    </div>
  );
};

export default App;
